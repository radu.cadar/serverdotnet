﻿using Microsoft.EntityFrameworkCore;
using ServerDotNet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerDotNet.DataLayer
{
    public class UniversityContext : DbContext
    {

       public UniversityContext(DbContextOptions dbContextOptions) : base (dbContextOptions)
        {

        }

        public virtual DbSet<StudentModel> Students { get; set; }

        public virtual DbSet<FacultyModel> Faculties { get; set; }
    }
}
