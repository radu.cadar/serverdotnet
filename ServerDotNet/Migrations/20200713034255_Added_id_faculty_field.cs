﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServerDotNet.Migrations
{
    public partial class Added_id_faculty_field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Id_Faculty",
                table: "Students",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Id_Faculty",
                table: "Students");
        }
    }
}
