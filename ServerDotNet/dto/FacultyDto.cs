﻿using ServerDotNet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerDotNet.dto
{
    public class FacultyDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<StudentModel> Students { get; set; }
    }
}
